#!/bin/bash

valac test.vala --vapidir=moxavapi --pkg iologik_e -C

clang test.c -lmxio_x64 `pkg-config --cflags --libs glib-2.0` `pkg-config --cflags --libs gobject-2.0` -o test

rm test.c
