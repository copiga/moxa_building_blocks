using iologik_e;

int main()
{
	status_codes.init();	
	int a = 0;
	stdout.printf(status_codes.check_error(MXEIO.init()));
	stdout.printf(status_codes.check_error(MXEIO.connect("192.168.123.123",1,1,&a)));
	MXEIO.exit();
	
	return a;
}

/*code seems correct. GCC has a bug which stops this from ccompiling on and laika. this may be a bug in GCC 4.7 as it compiles with clang*/

/*notes:*/
/*DO NOT use 127.0.0.1 as a test ip as it will sigpipe and leave us with many confus*/