namespace iologik_e
{
	class status_codes
	{
		public static int ok;

		public static int illegal_function;
		public static int illegal_data_address;
		public static int illegal_data_value;
		public static int slave_device_failure;
		public static int slave_device_busy;
		
		public static int eio_timeout;
		public static int eio_init_sockets_fail;
		public static int eio_creating_socket_error;
		public static int eio_response_bad;
		public static int eio_socket_disconnect;
		public static int eio_protocol_type_error;
		public static int eio_password_incorrect;

		public static int sio_open_fail;
		public static int sio_timeout;
		public static int sio_close_fail;
		public static int sio_purge_comm_fail;
		public static int sio_flush_file_buffers_fail;
		public static int sio_get_comm_state_fail;
		public static int sio_set_comm_state_fail;
		public static int sio_setup_comm_fail;
		public static int sio_set_comm_timeout_fail;
		public static int sio_clear_comm_fail;
		public static int sio_response_bad;
		public static int sio_transmission_mode_error;
		public static int sio_baud_rate_not_supported;

		public static int product_not_supported;
		public static int handle_error;
		public static int slot_out_of_range;
		public static int channel_out_of_range;
		public static int coil_type_error;
		public static int register_type_error;
		public static int function_not_supported;
		public static int output_value_out_of_range;
		public static int input_value_out_of_range;
		public static int slot_not_exist;
		public static int firmware_not_support;
		public static int create_mutex_fail;

		public static int enum_net_interface_fail;
		public static int add_info_table_fail;
		public static int unknown_net_interface_fail;
		public static int table_net_interface_fail;

		public static void init()
		{
			ok = 0;
			
			illegal_function = 1001;
			illegal_data_address = 1002;
			illegal_data_value = 1003;
			slave_device_failure = 1004;
			slave_device_busy = 1006;
		
			eio_timeout = 2001;
			eio_init_sockets_fail = 2002;
			eio_creating_socket_error = 2003;
			eio_response_bad = 2004;
			eio_socket_disconnect = 2005;
			eio_protocol_type_error = 2006;
			eio_password_incorrect = 2007;

			sio_open_fail = 3001;
			sio_timeout = 3002;
			sio_close_fail = 3003;
			sio_purge_comm_fail = 3004;
			sio_flush_file_buffers_fail = 3005;
			sio_get_comm_state_fail = 3006;
			sio_set_comm_state_fail = 3007;
			sio_setup_comm_fail = 3008;
			sio_set_comm_timeout_fail = 3009;
			sio_clear_comm_fail = 3010;
			sio_response_bad = 3011;
			sio_transmission_mode_error = 3012;
			sio_baud_rate_not_supported = 3013;

			product_not_supported = 4001;
			handle_error = 4002;
			slot_out_of_range = 4003;
			channel_out_of_range = 4004;
			coil_type_error = 4005;
			register_type_error = 4006;
			function_not_supported = 4007;
			output_value_out_of_range = 4008;
			input_value_out_of_range = 4009;
			slot_not_exist = 4010;
			firmware_not_support = 4011;
			create_mutex_fail = 4012;

			enum_net_interface_fail = 5000;
			add_info_table_fail = 5001;
			unknown_net_interface_fail = 5002;
			table_net_interface_fail = 5003;
		}

		
		public static string check_error(int retcode)
		{
			if(retcode == status_codes.ok)
				return "ok";
			else if(retcode == status_codes.illegal_function)
				return "illegal_function";
			else if(retcode == status_codes.illegal_data_address)
				return "illegal_data_address";
			else if(retcode == status_codes.illegal_data_value)
				return "illegal_data_value";
			else if(retcode == status_codes.slave_device_failure)
				return "slave_device_failure";
			else if(retcode == status_codes.slave_device_busy)
				return "slave_device_busy";
			else if(retcode == status_codes.eio_timeout)
				return "eio_timeout";
			else if(retcode == status_codes.eio_init_sockets_fail)
				return "eio_init_sockets_fail";
			else if(retcode == status_codes.eio_creating_socket_error)
				return "eio_creating_socket_error";
			else if(retcode == status_codes.eio_response_bad)
				return "eio_response_bad";
			else if(retcode == status_codes.eio_socket_disconnect)
				return "eio_socket_disconnect";
			else if(retcode == status_codes.eio_protocol_type_error)
				return "eio_protocol_type_error";
			else if(retcode == status_codes.eio_password_incorrect)
				return "eio_password_incorrect";
			else if(retcode == status_codes.sio_open_fail)
				return "sio_open_fail";
			else if(retcode == status_codes.sio_timeout)
				return "sio_timeout";
			else if(retcode == status_codes.sio_close_fail)
				return "sio_close_fail";
			else if(retcode == status_codes.sio_purge_comm_fail)
				return "sio_purge_comm_fail";
			else if(retcode == status_codes.sio_flush_file_buffers_fail)
				return "sio_flush_file_buffers_fail";
			else if(retcode == status_codes.sio_get_comm_state_fail)
				return "sio_get_comm_state_fail";
			else if(retcode == status_codes.sio_set_comm_state_fail)
				return "sio_set_comm_state_fail";
			else if(retcode == status_codes.sio_setup_comm_fail)
				return "sio_setup_comm_fail";
			else if(retcode == status_codes.sio_set_comm_timeout_fail)
				return "sio_set_comm_timeout_fail";
			else if(retcode == status_codes.sio_clear_comm_fail)
				return "sio_clear_comm_fail";
			else if(retcode == status_codes.sio_response_bad)
				return "sio_response_bad";
			else if(retcode == status_codes.sio_transmission_mode_error)
				return "sio_transmission_mode_error";
			else if(retcode == status_codes.sio_baud_rate_not_supported)
				return "sio_baud_rate_not_supported";
			else if(retcode == status_codes.product_not_supported)
				return "product_not_supported";
			else if(retcode == status_codes.handle_error)
				return "handle_error";
			else if(retcode == status_codes.slot_out_of_range)
				return "slot_out_of_range";
			else if(retcode == status_codes.channel_out_of_range)
				return "channel_out_of_range";
			else if(retcode == status_codes.coil_type_error)
				return "coil_type_error";
			else if(retcode == status_codes.register_type_error)
				return "register_type_error";
			else if(retcode == status_codes.function_not_supported)
				return "function_not_supported";
			else if(retcode == status_codes.output_value_out_of_range)
				return "output_value_out_of_range";
			else if(retcode == status_codes.input_value_out_of_range)
				return "input_value_out_of_range";
			else if(retcode == status_codes.slot_not_exist)
			return "slot_not_exist";
			else if(retcode == status_codes.firmware_not_support)
				return "firmware_not_support";
			else if(retcode == status_codes.create_mutex_fail)
				return "create_mutex_fail";
			else if(retcode == status_codes.enum_net_interface_fail)
				return "enum_net_interface_fail";
			else if(retcode == status_codes.add_info_table_fail)
				return "add_info_table_fail";
			else if(retcode == status_codes.unknown_net_interface_fail)
				return "unknown_net_interface_fail";
			else if(retcode == status_codes.table_net_interface_fail)
				return "table_net_interface_fail";
			else
				return "you done goofed";
		}
	}
}
